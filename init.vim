set number
set tabstop=4
set softtabstop=4
set shiftwidth=4
set noexpandtab
set numberwidth=1
syntax on


set termguicolors

let g:sonokai_style = 'andromeda'
let g:sonokai_enable_italic = 1
let g:sonokai_disable_italic_comment = 0
let g:sonokai_transparent_background = 1
let g:lightline = {'colorscheme' : 'sonokai'}

source /home/a/.vim/plugged/sonokai/colors/sonokai.vim
"colorscheme sonokai

highlight CursorLineNr cterm=NONE ctermbg=15 ctermfg=0 gui=NONE guibg=#ffffff guifg=#d70000
set cursorline
autocmd ColorScheme * highlight CursorLineNr cterm=bold term=bold gui=bold

call plug#begin('~/.vim/plugged')
if has('nvim')
	Plug 'Shougo/deoplete.nvim', {'do': ':UpdateRemotePlugins'}
else
	Plug 'Shougo/deoplete.nvim'
	Plug 'roxma/nvim-yarp'
	Plug 'roxma/vim-hug-neovim-rpc'
endif
let g:deoplete#enable_at_startup = 1
Plug 'junegunn/vim-easy-align'
Plug 'metakirby5/codi.vim'
Plug 'rstacruz/vim-closer'
Plug 'sainnhe/sonokai'
Plug 'itchyny/lightline.vim'
Plug 'sheerun/vim-polyglot'
Plug 'farmergreg/vim-lastplace'

call plug#end()

xmap ga <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)
